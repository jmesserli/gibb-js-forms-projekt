﻿$(function () {
    var form = $("#login-register-form");

    $("#button-login").click(function () {
        if (validateFormLogin()) {
            form.append($("<input>").attr("type", "hidden").attr("name", "isLogin").val("true"));
            form.submit();
        }
    });

    $("#button-register").click(function () {
        if (validateFormRegister()){
            form.append($("<input>").attr("type", "hidden").attr("name", "isLogin").val("false"));
            form.submit();
        }
    });

    $("#button-reset").click(function () {
        clearWarnings();
    })
});

var email_regex = /^[-a-z0-9~!$%^&*_=+}{'?]+(\.[-a-z0-9~!$%^&*_=+}{'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.([a-z]{2,9})|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
var password_regex = /^(?=.*[A-Z].*)(?=.*[a-z].*)(?=.*\d.*)(?=.*[\[\]{}()\-_*%~`$!?#].*).{8,}$/;

function validateEmail() {
    var $email = $("#input-email");

    if (!email_regex.test($email.val())) {
        addWarning($email, "ungültige E-Mail Adresse");
        return false;
    } else {
        removeWarning($email);
        return true;
    }
}

function validatePassword() {
    var $password = $("#input-password");

    if (!password_regex.test($password.val())) {
        addWarning($password, "Passwort muss Gross- und Kleinbuchstaben, Zahlen und Sonderzeichen enthalten und mindestens 8 Zeichen lang sein");
        return false;
    } else {
        removeWarning($password);
        return true;
    }
}

function validatePasswordRepeat() {
    var $password = $("#input-password");
    var $password_repeat = $("#input-password-repeat");

    if ($password.val() != $password_repeat.val()) {
        addWarning($password_repeat, "Passwörter stimmen nicht überein");
        return false;
    } else {
        removeWarning($password_repeat);
        return true;
    }
}

function validateSex() {
    var $selected = $("input[name=sex]:checked");
    var $sex_input_group = $("#input-sex-group");

    if ($selected.length < 1) {
        addWarning($sex_input_group, "Es muss ein Geschlecht ausgewählt werden");
        return false;
    } else {
        removeWarning($sex_input_group);
        return true;
    }
}

function validateInterests() {
    var $checked = $("input[name=interests\\[\\]]:checked");
    var $interests_input_group = $("#input-interests-group");

    if ($checked.length < 1) {
        addWarning($interests_input_group, "Es muss mindestens eines ausgewählt werden");
        return false;
    } else {
        removeWarning($interests_input_group);
        return true;
    }
}

function validateFormLogin() {
    var noErrors = true;

    clearWarnings();

    noErrors &= validateEmail();
    noErrors &= validatePassword();

    return noErrors;
}

function validateFormRegister() {
    var noErrors = true;

    noErrors &= validateEmail();
    noErrors &= validatePassword();
    noErrors &= validatePasswordRepeat();
    noErrors &= validateSex();
    noErrors &= validateInterests();

    return noErrors;
}

function clearWarnings() {
    var $warnings = $("div.warning");
    $warnings.slideUp("fast", function () {
        $warnings.remove();
    });
}

function hasWarning(input) {
    var $form_line = input.parents("div.form-line");
    var $next = $form_line.next();

    return $next.hasClass("warning");
}

function addWarning(input, warning) {
    var $form_line = input.parent("div.form-line");
    var $warning;

    if (hasWarning(input)) {
        $warning = $form_line.next();
        var $warning_p = $warning.children("p");
        $warning_p.html(warning);
        return;
    }

    $warning = $('<div class="form-line warning"><p>' + warning + '</p></div>');
    $warning.hide();
    $form_line.after($warning);
    $warning.slideDown("fast");
}

function removeWarning(input) {
    var $parent = input.parents("div.form-line");
    var $next = $parent.next();

    if ($next.hasClass("warning"))
        $next.slideUp("fast", function () {
            $next.remove();
        });
}