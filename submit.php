<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login / Registrierung</title>

    <style>
        * {
            font-family: sans-serif;
        }
    </style>

</head>
<body>
<?php

$email = filter_input(INPUT_POST, "email", FILTER_VALIDATE_EMAIL);
$pass = filter_input(INPUT_POST, "password", FILTER_VALIDATE_REGEXP, array("options" => array("regexp" => "/^(?=.*[A-Z].*)(?=.*[a-z].*)(?=.*\\d.*)(?=.*[\\[\\]{}()\\-_*%~`$!?#].*).{8,}$/")));
$passre = filter_input(INPUT_POST, "password-repeat");
$sex = filter_input(INPUT_POST, "sex");
$interests = $_POST["interests"];
$car = filter_input(INPUT_POST, "car");

$login = filter_input(INPUT_POST, "isLogin", FILTER_VALIDATE_BOOLEAN);

if ($login) {
    if ($email == null || $pass == null) {
        echo "Ungültige E-Mail oder ungültiges Passwort";
        exit;
    }

    echo "<p>Login für Benutzer <b>$email</b> mit dem Passwort <b>$pass</b> ok.</p>";
} else {
    // Registrierung
    if ($email == null || $pass == null || $passre == null || $pass != $passre || $sex == null || $interests == null || $car == null){
        echo "<p>Mindestens ein ungültiger Wert. Bitte gehen Sie <a href='login_register.html'>Zurück</a>.</p>";
        if ($email == null)
            echo "<p>(Email ungültig)</p>";
        if ($pass == null)
            echo "<p>(Passwort ungültig)</p>";
        if ($passre == null)
            echo "<p>(Passwortwiederholung ungültig)</p>";
        if ($passre != $pass )
            echo "<p>(Passwortwiederholung stimmt nicht überein)</p>";
        if ($sex == null)
            echo "<p>(Geschlecht ungültig)</p>";
        if ($interests == null)
            echo "<p>(Interessen ungültig)</p>";
        if ($car == null)
            echo "<p>(Auto ungültig)</p>";
    }

    echo "<p>Registrierung für Benutzer <b>$email</b> mit dem Passwort <b>$pass</b> ok:</p>";
    echo "<p>Geschlecht: $sex</p>";
    $interests_joined = join(', ', $interests);
    echo "<p>Interessen: $interests_joined</p>";
    echo "<p>Auto: $car</p>";
}

?>
</body>
</html>